func testar_assign() returns void {
    asd as int = 0x2;
}

func testar_dor() returns bool {
    return false || true;
}

func testar_dand() returns bool {
    return false && true;
}

func testar_or() returns int {
    return 1 | 2;
}

func testar_and() returns int {
    return 1 & 2;
}

func testar_igual() returns bool {
    return testar_or() == testar_and();
}

func testar_diferente() returns bool {
    return testar_or() != testar_and();
}

func testar_menorq() returns bool {
    return testar_and() < testar_or() ;
}

func testar_maiorq() returns bool {
    return testar_and() > testar_or() ;
}

func testar_menorigualq() returns bool {
    return testar_and() <= testar_or() ;
}

func testar_maiorigualq() returns bool {
    return testar_and() >= testar_or() ;
}

func testar_mais() returns int {
    return 1 + 1 + 1;
}

func testar_menos() returns int {
    return 5 - 3 - 2;
}

func testar_multi() returns int {
    return 2 * 3;
}

func testar_div() returns int {
    return 3 / 2;
}

func testar_mod() returns int {
    return 11 % 2;
}

func testar_not() returns bool {
    return !false;
}

func testar_bitnot() returns int {
    return ~0x0fffffffffffffff;
}

func testar(arg as int, arg2 as bool) returns char {
    if(arg2) {
        printk(arg+1);
    } else {
        printk(arg);
    }

    while(arg >= 0 && arg2) {
        printk(arg);
        if(arg == 2) {
          break;
        }

        arg = arg - 1;
        continue;
    }

    printk('a');
    return 'b';
}


func testar_recurssao(c as int) returns void {
    if(c == 0) {
        return;
    }

    testar_recurssao(c-1);
}

func start() returns void {
    printk("ok\n");
    
    (testar_assign());printk("\n");
    printk(testar_dor());printk("\n");
    printk(testar_dand());printk("\n");
    printk(testar_or());printk("\n");
    printk(testar_and());printk("\n");
    printk(testar_igual());printk("\n");
    printk(testar_diferente());printk("\n");
    printk(testar_menorq());printk("\n");
    printk(testar_maiorq());printk("\n");
    printk(testar_menorigualq());printk("\n");
    printk(testar_maiorigualq());printk("\n");
    printk(testar_mais());printk("\n");
    printk(testar_menos());printk("\n");
    printk(testar_multi());printk("\n");
    printk(testar_div());printk("\n");
    printk(testar_mod());printk("\n");
    printk(testar_not());printk("\n");
    printk(testar_bitnot());printk("\n");
    testar_recurssao(10);

    a1 as int = 2 * 5 + 10 - 2 / 2;
    a2 as bool = true;
    r as char = testar(a1+1, a2);
}
