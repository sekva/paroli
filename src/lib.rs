#[macro_use]
extern crate lazy_static;

mod ast;
mod checker;
mod lexer;
mod parser;
mod token;
mod util;

use std::{str::FromStr, sync::Mutex};

use checker::checar_integridade;
use lexer::lexer;
use parser::gerar_ast;

lazy_static! {
    pub(crate) static ref NOME_ARQUIVO: Mutex<Option<String>> = Mutex::new(None);
    pub(crate) static ref LINHAS: Mutex<Vec<String>> = Mutex::new(Vec::new());
}

pub fn compilar(arq: &str) {
    let arquivo = std::fs::read_to_string(arq).expect(&format!("Não foi possivel ler {}", arq));
    let arquivo_linhas: Vec<String> = arquivo
        .lines()
        .map(|s| String::from_str(s).unwrap())
        .collect();

    LINHAS.lock().unwrap().extend(arquivo_linhas.into_iter());
    NOME_ARQUIVO.lock().unwrap().replace(arq.into());

    let tokens = lexer(&arquivo);
    let ast = gerar_ast(tokens);
    checar_integridade(&ast);
}
