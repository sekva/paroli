use crate::{
    token::{Localizacao, TipoToken, Token},
    LINHAS, NOME_ARQUIVO,
};

fn printar_msg(tipo: &str, loc: Localizacao, msg: &str) {
    let nome_arq: Option<String> = NOME_ARQUIVO.lock().unwrap().clone();
    let arq: Vec<String> = LINHAS.lock().unwrap().clone();

    println!(
        "--> {}:{}:{}: {}: {}",
        nome_arq.unwrap(),
        loc.linha,
        loc.coluna,
        tipo,
        msg
    );
    println!();
    let linha = arq[loc.linha - 1].clone();
    let num_linha = format!("{}|  ", loc.linha);
    println!("{}{}", num_linha, linha);

    for _ in 0..num_linha.len() {
        print!(" ");
    }

    for _ in 1..loc.coluna {
        print!(" ");
    }

    print!("^");
    println!();
    println!();
}

pub fn _aviso(loc: Localizacao, msg: &str) {
    printar_msg("aviso", loc, msg);
}

pub fn erro(loc: Localizacao, msg: &str, notas: &[&str]) -> ! {
    printar_msg("erro", loc, msg);

    for nota in notas {
        println!("nota: {}", nota);
    }

    std::process::exit(1);
}

pub(crate) fn check_token(token_esperado: TipoToken, tokens: &mut Vec<Token>) -> Token {
    if let Some(t) = tokens.pop() {
        if token_esperado == t.tipo {
            return t;
        }

        erro(
            t.localizacao,
            &format!("`{}` esperado, mas `{}` foi encontrado", token_esperado, t),
            &[],
        );
    }

    erro(
        Localizacao {
            linha: 1,
            coluna: 0,
        },
        "Fim inesperado do arquivo",
        &[],
    );
}

pub(crate) fn peek_token(token_esperado: TipoToken, tokens: &Vec<Token>) -> bool {
    if let Some(t) = tokens.last() {
        if token_esperado == t.tipo {
            return true;
        }
    }
    false
}
