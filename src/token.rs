use std::fmt::Display;

#[derive(Debug, Clone)]
pub enum TipoToken {
    ParenteseEsquerdo,
    ParenteseDireito,

    ChaveEsquerda,
    ChaveDireita,

    Virgula,
    PontoVirgula,

    Assign,

    DOr,
    DAnd,

    Or,
    And,

    Mais,
    Menos,

    MenorQ,
    MaiorQ,
    MenorIgualQ,
    MaiorIgualQ,

    Multi,
    Div,
    Mod,

    Not,
    BitNot,

    Igual,
    Diferente,

    ID(String),
    STR(String),
    CHAR(char),
    INT(i64),
    BOOL(bool),
    FLOAT(f64),

    ELSE,
    FUNC,
    IF,
    PRINTK,
    RETURN,
    RETURNS,
    AS,
    WHILE,
    BREAK,
    CONTINUE,

    EOF,
}

impl TipoToken {
    pub(crate) fn lexema_igual(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::ID(_l0), Self::ID(_r0)) => _l0 == _r0,
            (Self::STR(_l0), Self::STR(_r0)) => _l0 == _r0,
            (Self::CHAR(_l0), Self::CHAR(_r0)) => _l0 == _r0,
            (Self::INT(_l0), Self::INT(_r0)) => _l0 == _r0,
            (Self::BOOL(_l0), Self::BOOL(_r0)) => _l0 == _r0,
            (Self::FLOAT(_l0), Self::FLOAT(_r0)) => _l0 == _r0,
            _ => core::mem::discriminant(self) == core::mem::discriminant(other),
        }
    }
}

impl PartialEq for TipoToken {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::ID(_l0), Self::ID(_r0)) => true,
            (Self::STR(_l0), Self::STR(_r0)) => true,
            (Self::CHAR(_l0), Self::CHAR(_r0)) => true,
            (Self::INT(_l0), Self::INT(_r0)) => true,
            (Self::BOOL(_l0), Self::BOOL(_r0)) => true,
            (Self::FLOAT(_l0), Self::FLOAT(_r0)) => true,
            _ => core::mem::discriminant(self) == core::mem::discriminant(other),
        }
    }
}

impl Display for TipoToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let ret_str: String = match self {
            TipoToken::ParenteseEsquerdo => "(".into(),
            TipoToken::ParenteseDireito => ")".into(),
            TipoToken::ChaveEsquerda => "{".into(),
            TipoToken::ChaveDireita => "}".into(),
            TipoToken::Virgula => ",".into(),
            TipoToken::PontoVirgula => ";".into(),
            TipoToken::Assign => "=".into(),
            TipoToken::DOr => "||".into(),
            TipoToken::DAnd => "&&".into(),
            TipoToken::Or => "|".into(),
            TipoToken::And => "&".into(),
            TipoToken::Mais => "+".into(),
            TipoToken::Menos => "-".into(),
            TipoToken::MenorQ => "<".into(),
            TipoToken::MaiorQ => ">".into(),
            TipoToken::MenorIgualQ => ">=".into(),
            TipoToken::MaiorIgualQ => "<=".into(),
            TipoToken::Multi => "*".into(),
            TipoToken::Div => "/".into(),
            TipoToken::Mod => "%".into(),
            TipoToken::Not => "!".into(),
            TipoToken::BitNot => "~".into(),
            TipoToken::Igual => "==".into(),
            TipoToken::Diferente => "!=".into(),
            TipoToken::ID(v) => v.into(),
            TipoToken::STR(v) => v.into(),
            TipoToken::CHAR(v) => format!("{}", v),
            TipoToken::INT(v) => format!("{}", v),
            TipoToken::BOOL(v) => format!("{}", v),
            TipoToken::FLOAT(v) => format!("{}", v),
            TipoToken::ELSE => "else".into(),
            TipoToken::FUNC => "func".into(),
            TipoToken::IF => "if".into(),
            TipoToken::PRINTK => "printk".into(),
            TipoToken::RETURN => "return".into(),
            TipoToken::RETURNS => "returns".into(),
            TipoToken::AS => "as".into(),
            TipoToken::WHILE => "while".into(),
            TipoToken::BREAK => "break".into(),
            TipoToken::CONTINUE => "continue".into(),
            TipoToken::EOF => "".into(),
        };

        write!(f, "{}", ret_str)
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Localizacao {
    pub linha: usize,
    pub coluna: usize,
}

impl Default for Localizacao {
    fn default() -> Self {
        Self {
            linha: 1,
            coluna: 0,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Token {
    pub localizacao: Localizacao,
    pub tipo: TipoToken,
}

impl Token {
    pub fn eh_literal(&self) -> bool {
        match self.tipo {
            TipoToken::STR(_) => true,
            TipoToken::CHAR(_) => true,
            TipoToken::INT(_) => true,
            TipoToken::BOOL(_) => true,
            TipoToken::FLOAT(_) => true,
            _ => false,
        }
    }

    pub fn tipo_str(&self) -> String {
        if self.eh_literal() {
            match self.tipo {
                TipoToken::STR(_) => {
                    return "str".into();
                }
                TipoToken::CHAR(_) => {
                    return "char".into();
                }
                TipoToken::INT(_) => {
                    return "int".into();
                }
                TipoToken::BOOL(_) => {
                    return "bool".into();
                }
                TipoToken::FLOAT(_) => {
                    return "float".into();
                }
                _ => {}
            }
        }

        panic!("Deveria ter checado antes de colher o tipo...");
    }
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.tipo)
    }
}
