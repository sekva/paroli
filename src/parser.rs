use std::sync::Mutex;

use crate::{
    ast::{Ast, Corpo, Expr, Func, Stmt},
    token::{Localizacao, TipoToken, Token},
    util::{check_token, erro, peek_token},
};

lazy_static! {
    pub(crate) static ref STACK_WHILE: Mutex<Vec<()>> = Mutex::new(vec![]);
}

fn parse_expr_val(tokens: &mut Vec<Token>) -> Expr {
    let id = tokens.pop().unwrap();
    let mut params = Vec::new();
    let eh_funcao = peek_token(TipoToken::ParenteseEsquerdo, tokens);

    if eh_funcao {
        tokens.pop();

        if !peek_token(TipoToken::ParenteseDireito, tokens) {
            loop {
                params.push(parse_expr(tokens));

                if !peek_token(TipoToken::Virgula, tokens) {
                    break;
                } else {
                    tokens.pop(); // Pop a ,
                }
            }
        }

        check_token(TipoToken::ParenteseDireito, tokens);
    }

    Expr::Chamada {
        id,
        eh_funcao,
        params,
    }
}

fn parse_expr_un(tokens: &mut Vec<Token>) -> Expr {
    if peek_token(TipoToken::ParenteseEsquerdo, tokens) {
        return parse_expr(tokens);
    }

    if peek_token(TipoToken::BitNot, tokens)
        || peek_token(TipoToken::Not, tokens)
        || peek_token(TipoToken::Menos, tokens)
    {
        let operador = tokens.pop().unwrap();
        let operando = Box::new(parse_expr(tokens));
        return Expr::Unaria { operador, operando };
    }

    if peek_token(TipoToken::ID("".into()), tokens)
        || peek_token(TipoToken::STR("".into()), tokens)
        || peek_token(TipoToken::CHAR('0'), tokens)
        || peek_token(TipoToken::INT(0), tokens)
        || peek_token(TipoToken::BOOL(false), tokens)
        || peek_token(TipoToken::FLOAT(0.0), tokens)
    {
        return parse_expr_val(tokens);
    }

    if let Some(t) = tokens.pop() {
        erro(
            t.localizacao,
            &format!(
                "`id`, `str`, `char`, `int`, `bool` ou `float` eram esperados, mas `{:?}` foi encontrado", t.tipo
            ),&[]
        );
    } else {
        erro(
            Localizacao {
                linha: 1,
                coluna: 0,
            },
            "Fim prematuro do arquivo",
            &[],
        )
    }
}

fn parse_expr_mult(tokens: &mut Vec<Token>) -> Expr {
    let mut lhs = parse_expr_un(tokens);

    while peek_token(TipoToken::Multi, tokens)
        || peek_token(TipoToken::Div, tokens)
        || peek_token(TipoToken::Mod, tokens)
    {
        let op = tokens.pop().unwrap();
        let rhs = parse_expr_un(tokens);
        lhs = Expr::Calc {
            lhs: Box::new(lhs),
            op,
            rhs: Box::new(rhs),
        };
    }

    return lhs;
}

fn parse_expr_add(tokens: &mut Vec<Token>) -> Expr {
    let mut lhs = parse_expr_mult(tokens);

    while peek_token(TipoToken::Mais, tokens) || peek_token(TipoToken::Menos, tokens) {
        let op = tokens.pop().unwrap();
        let rhs = parse_expr_mult(tokens);
        lhs = Expr::Calc {
            lhs: Box::new(lhs),
            op,
            rhs: Box::new(rhs),
        };
    }

    return lhs;
}

fn parse_expr_cmp(tokens: &mut Vec<Token>) -> Expr {
    let mut lhs = parse_expr_add(tokens);

    while peek_token(TipoToken::MenorQ, tokens)
        || peek_token(TipoToken::MaiorQ, tokens)
        || peek_token(TipoToken::MaiorIgualQ, tokens)
        || peek_token(TipoToken::MenorIgualQ, tokens)
    {
        let op = tokens.pop().unwrap();
        let rhs = parse_expr_add(tokens);
        lhs = Expr::Calc {
            lhs: Box::new(lhs),
            op,
            rhs: Box::new(rhs),
        };
    }

    return lhs;
}

fn parse_expr_eq_neq(tokens: &mut Vec<Token>) -> Expr {
    let mut lhs = parse_expr_cmp(tokens);

    while peek_token(TipoToken::Igual, tokens) || peek_token(TipoToken::Diferente, tokens) {
        let op = tokens.pop().unwrap();
        let rhs = parse_expr_cmp(tokens);
        lhs = Expr::Calc {
            lhs: Box::new(lhs),
            op,
            rhs: Box::new(rhs),
        };
    }

    return lhs;
}

fn parse_expr_bor_band(tokens: &mut Vec<Token>) -> Expr {
    let mut lhs = parse_expr_eq_neq(tokens);

    while peek_token(TipoToken::And, tokens) || peek_token(TipoToken::Or, tokens) {
        let op = tokens.pop().unwrap();
        let rhs = parse_expr_eq_neq(tokens);
        lhs = Expr::Calc {
            lhs: Box::new(lhs),
            op,
            rhs: Box::new(rhs),
        };
    }

    return lhs;
}

fn parse_expr_or_and(tokens: &mut Vec<Token>) -> Expr {
    let mut lhs = parse_expr_bor_band(tokens);

    while peek_token(TipoToken::DAnd, tokens) || peek_token(TipoToken::DOr, tokens) {
        let op = tokens.pop().unwrap();
        let rhs = parse_expr_bor_band(tokens);
        lhs = Expr::Calc {
            lhs: Box::new(lhs),
            op,
            rhs: Box::new(rhs),
        };
    }

    return lhs;
}

fn parse_expr(tokens: &mut Vec<Token>) -> Expr {
    if peek_token(TipoToken::ParenteseEsquerdo, tokens) {
        tokens.pop();
        let ret = parse_expr(tokens);
        check_token(TipoToken::ParenteseDireito, tokens);
        return ret;
    }

    return parse_expr_or_and(tokens);
}

fn parse_var_assgn(tokens: &mut Vec<Token>) -> Stmt {
    let nome = check_token(TipoToken::ID("".into()), tokens);
    let loc = check_token(TipoToken::Assign, tokens).localizacao;
    let valor = parse_expr(tokens);
    check_token(TipoToken::PontoVirgula, tokens);

    Stmt::VarAssgn { loc, nome, valor }
}

fn parse_var_decl(tokens: &mut Vec<Token>) -> Stmt {
    let nome = check_token(TipoToken::ID("".into()), tokens);
    check_token(TipoToken::AS, tokens);
    let tipo = check_token(TipoToken::ID("".into()), tokens);
    let loc = check_token(TipoToken::Assign, tokens).localizacao;
    let valor = parse_expr(tokens);
    check_token(TipoToken::PontoVirgula, tokens);

    Stmt::VarDecl {
        loc,
        nome,
        tipo,
        valor,
    }
}

fn parse_printk(tokens: &mut Vec<Token>) -> Stmt {
    check_token(TipoToken::PRINTK, tokens);
    let loc = check_token(TipoToken::ParenteseEsquerdo, tokens).localizacao;
    let expr = parse_expr(tokens);
    check_token(TipoToken::ParenteseDireito, tokens);
    check_token(TipoToken::PontoVirgula, tokens);

    Stmt::Printk(loc, expr)
}

fn parse_while(tokens: &mut Vec<Token>) -> Stmt {
    check_token(TipoToken::WHILE, tokens);
    let loc = check_token(TipoToken::ParenteseEsquerdo, tokens).localizacao;
    let condicao = parse_expr(tokens);
    check_token(TipoToken::ParenteseDireito, tokens);

    // {
    let bloco = parse_corpo(tokens);
    // }

    Stmt::While {
        loc,
        condicao,
        bloco,
    }
}

fn parse_if(tokens: &mut Vec<Token>) -> Stmt {
    check_token(TipoToken::IF, tokens);
    let loc = check_token(TipoToken::ParenteseEsquerdo, tokens).localizacao;
    let condicao = parse_expr(tokens);
    check_token(TipoToken::ParenteseDireito, tokens);

    let bloco_true = parse_corpo(tokens);
    let mut bloco_false = None;

    if peek_token(TipoToken::ELSE, tokens) {
        tokens.pop();

        bloco_false = Some(parse_corpo(tokens));
    }

    Stmt::If {
        loc,
        condicao,
        bloco_true,
        bloco_false,
    }
}

fn parse_return(tokens: &mut Vec<Token>) -> Stmt {
    let t = check_token(TipoToken::RETURN, tokens);
    if peek_token(TipoToken::PontoVirgula, tokens) {
        tokens.pop();
        return Stmt::Return(t.localizacao, None);
    }

    let expr = Stmt::Return(t.localizacao, Some(parse_expr(tokens)));
    check_token(TipoToken::PontoVirgula, tokens);
    return expr;
}

fn parse_stmt(tokens: &mut Vec<Token>) -> Option<Stmt> {
    if peek_token(TipoToken::IF, tokens) {
        return Some(parse_if(tokens));
    }

    if peek_token(TipoToken::PRINTK, tokens) {
        return Some(parse_printk(tokens));
    }

    if peek_token(TipoToken::RETURN, tokens) {
        return Some(parse_return(tokens));
    }

    if peek_token(TipoToken::WHILE, tokens) {
        STACK_WHILE.lock().unwrap().push(());
        let ret = Some(parse_while(tokens));
        let _ = STACK_WHILE.lock().unwrap().pop();
        return ret;
    }

    let tamanho_stack_while: usize = STACK_WHILE.lock().unwrap().len();

    if peek_token(TipoToken::BREAK, tokens) {
        if tamanho_stack_while == 0 {
            erro(
                check_token(TipoToken::BREAK, tokens).localizacao,
                "´break´ fora de um while",
                &[],
            );
        }
        tokens.pop();
        check_token(TipoToken::PontoVirgula, tokens);

        return Some(Stmt::Break);
    }

    if peek_token(TipoToken::CONTINUE, tokens) {
        if tamanho_stack_while == 0 {
            erro(
                check_token(TipoToken::CONTINUE, tokens).localizacao,
                "´continue´ fora de um while",
                &[],
            );
        }
        tokens.pop();
        check_token(TipoToken::PontoVirgula, tokens);

        return Some(Stmt::Continue);
    }

    if peek_token(TipoToken::ParenteseEsquerdo, tokens) {
        tokens.pop();
        let ret = parse_expr(tokens);
        check_token(TipoToken::ParenteseDireito, tokens);
        check_token(TipoToken::PontoVirgula, tokens);
        return Some(Stmt::ExprVazia(ret));
    }

    if peek_token(TipoToken::ID("".into()), tokens) {
        let id = tokens.pop().unwrap();

        if peek_token(TipoToken::AS, tokens) {
            tokens.push(id);
            return Some(parse_var_decl(tokens));
        }

        if peek_token(TipoToken::Assign, tokens) {
            tokens.push(id);
            return Some(parse_var_assgn(tokens));
        }

        tokens.push(id);

        let ret = Some(Stmt::ExprVazia(parse_expr(tokens)));
        check_token(TipoToken::PontoVirgula, tokens);
        return ret;
    }

    if peek_token(TipoToken::Menos, tokens)
        || peek_token(TipoToken::Not, tokens)
        || peek_token(TipoToken::BitNot, tokens)
    {
        let ret = Some(Stmt::ExprVazia(parse_expr(tokens)));
        check_token(TipoToken::PontoVirgula, tokens);
        return ret;
    }

    None
}

fn parse_corpo(tokens: &mut Vec<Token>) -> Corpo {
    let mut stmts = Vec::new();

    check_token(TipoToken::ChaveEsquerda, tokens);

    while let Some(stmt) = parse_stmt(tokens) {
        stmts.push(stmt);
    }

    check_token(TipoToken::ChaveDireita, tokens);

    Corpo { stmts }
}

fn parse_args(tokens: &mut Vec<Token>) -> Vec<(Token, Token)> {
    check_token(TipoToken::ParenteseEsquerdo, tokens);

    let mut args = Vec::new();

    while peek_token(TipoToken::ID("".into()), tokens) {
        let nome = tokens.pop().unwrap();
        check_token(TipoToken::AS, tokens);
        let tipo = check_token(TipoToken::ID("".into()), tokens);
        args.push((nome, tipo));

        if peek_token(TipoToken::Virgula, tokens) {
            tokens.pop();
        }
    }

    check_token(TipoToken::ParenteseDireito, tokens);
    args
}

fn parse_func(tokens: &mut Vec<Token>) -> Func {
    check_token(TipoToken::FUNC, tokens);
    let nome = check_token(TipoToken::ID("".into()), tokens);
    let args = parse_args(tokens);
    check_token(TipoToken::RETURNS, tokens);
    let ret = check_token(TipoToken::ID("".into()), tokens);
    let corpo = parse_corpo(tokens);

    Func {
        nome,
        args,
        ret,
        corpo,
    }
}

pub(crate) fn gerar_ast(mut tokens: Vec<Token>) -> Ast {
    tokens.reverse();

    let mut funcs = Vec::new();

    while let Some(token) = tokens.last() {
        match token.tipo {
            TipoToken::FUNC => {
                funcs.push(parse_func(&mut tokens));
            }
            TipoToken::EOF => {
                tokens.pop();
            }
            _ => {
                erro(token.localizacao, "Token Inesperado", &[]);
            }
        }
    }

    Ast { funcs }
}
