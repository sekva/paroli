use crate::token::{Localizacao, Token};

#[derive(Debug, Clone)]
pub(crate) struct Ast {
    pub funcs: Vec<Func>,
}

#[derive(Debug, Clone)]
pub(crate) struct Func {
    pub nome: Token,
    pub args: Vec<(Token, Token)>,
    pub ret: Token,
    pub corpo: Corpo,
}

#[derive(Debug, Clone)]
pub(crate) struct Corpo {
    pub stmts: Vec<Stmt>,
}

#[derive(Debug, Clone)]
pub(crate) enum Stmt {
    VarDecl {
        loc: Localizacao,
        nome: Token,
        tipo: Token,
        valor: Expr,
    },

    VarAssgn {
        loc: Localizacao,
        nome: Token,
        valor: Expr,
    },

    If {
        loc: Localizacao,
        condicao: Expr,
        bloco_true: Corpo,
        bloco_false: Option<Corpo>,
    },

    While {
        loc: Localizacao,
        condicao: Expr,
        bloco: Corpo,
    },
    Break,
    Continue,

    Printk(Localizacao, Expr),
    Return(Localizacao, Option<Expr>),
    ExprVazia(Expr),
}

#[derive(Debug, Clone)]
pub(crate) enum Expr {
    Chamada {
        id: Token,
        params: Vec<Expr>,
        eh_funcao: bool,
    },

    Unaria {
        operador: Token,
        operando: Box<Expr>,
    },

    Calc {
        lhs: Box<Expr>,
        op: Token,
        rhs: Box<Expr>,
    },
}
