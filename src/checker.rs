use std::fmt::Display;

use crate::{
    ast::{Ast, Corpo, Expr, Func, Stmt},
    token::{Localizacao, TipoToken, Token},
    util::erro,
};

#[derive(Debug)]
enum StackDefSimbolo {
    DivFrame,
    Simb(Token, Token),
}

impl Display for StackDefSimbolo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // TODO: melhorar
        write!(f, "{:?}", self)
    }
}

struct Contexto {
    ultimo_tipo: Option<Token>,
    tipo_ret: Token,
    simbolos_definidos: Vec<StackDefSimbolo>,
    retornou: bool,
    deve_retornar: bool,
}

fn desalocar_frame(contexto: &mut Contexto) {
    while let Some(StackDefSimbolo::Simb(_, _)) = contexto.simbolos_definidos.last() {
        // Remove todos os simbolos definidos ate a divisa do frame
        contexto.simbolos_definidos.pop();
    }

    if contexto.simbolos_definidos.len() > 1 {
        let s: StackDefSimbolo = contexto.simbolos_definidos.pop().unwrap();
        match &s {
            StackDefSimbolo::Simb(nome, _tipo) => {
                erro(
                    nome.localizacao,
                    format!("[BUG] Simbolo não desalocado: {}", s).as_str(),
                    &[],
                );
            }

            _ => {}
        }
    } else if contexto.simbolos_definidos.is_empty() {
        erro(
            Localizacao::default(),
            format!("[BUG] StackDefSimb mal manejada").as_str(),
            &[],
        );
    } else {
        contexto.simbolos_definidos.pop();
    }
}

fn checar_chamada_var_valor(contexto: &mut Contexto, id: &Token) {
    if id.eh_literal() {
        let ts = Token {
            localizacao: Localizacao::default(),
            tipo: TipoToken::ID(id.tipo_str()),
        };

        contexto.ultimo_tipo.replace(ts);
        return;
    }

    for var in contexto.simbolos_definidos.iter().rev() {
        if let StackDefSimbolo::Simb(nome, tipo) = var {
            if nome.tipo.lexema_igual(&id.tipo) {
                contexto.ultimo_tipo.replace(tipo.clone());
                return;
            }
        }
    }

    erro(
        id.localizacao,
        &format!("Variavel `{}` não definida no escopo", id),
        &[],
    );
}

fn checar_chamada_funcao(contexto: &mut Contexto, id: &Token, params: &Vec<Expr>, ast: &Ast) {
    for func in ast.funcs.iter() {
        if func.nome.tipo.lexema_igual(&id.tipo) {
            if func.args.len() != params.len() {
                erro(
                    id.localizacao,
                    &format!(
                        "A função `{}` espera {} argumentos, mas apenas {} foram passados",
                        id,
                        func.args.len(),
                        params.len()
                    ),
                    &[],
                );
            }

            let mut cnt = 0;
            for (passado, (_nome, esperado)) in params.iter().zip(func.args.iter()) {
                cnt += 1;
                checar_expr(contexto, passado, ast);
                let tipo_passado = contexto.ultimo_tipo.clone();

                if tipo_passado.is_none() {
                    erro(id.localizacao, &format!("O tipo esperado para o {}º argumento da função `{}` é `{}`, mas nenhum valor foi encontrado", cnt, id, esperado), &[]);
                }

                let tipo_passado = tipo_passado.unwrap();
                if !tipo_passado.tipo.lexema_igual(&esperado.tipo) {
                    erro(id.localizacao, &format!("O tipo esperado para o {}º argumento da função `{}` é `{}`, mas nenhum `{}` foi encontrado", cnt, id, esperado, tipo_passado), &[]);
                }
            }

            contexto.ultimo_tipo = Some(func.ret.clone());
            return;
        }
    }

    erro(
        id.localizacao,
        &format!("A função `{}` não foi encontrada", id),
        &[],
    );
}

fn checar_chamada(
    contexto: &mut Contexto,
    eh_funcao: bool,
    id: &Token,
    params: &Vec<Expr>,
    ast: &Ast,
) {
    if eh_funcao {
        checar_chamada_funcao(contexto, id, params, ast);
    } else {
        checar_chamada_var_valor(contexto, id);
    }
}

fn checar_unaria(contexto: &mut Contexto, operador: &Token, operando: &Expr, ast: &Ast) {
    checar_expr(contexto, operando, ast);
    let operando = contexto.ultimo_tipo.clone();

    if operando.is_none() {
        erro(
            operador.localizacao,
            "Operando da operação unaria não tem um valor",
            &[],
        );
    }

    let operando = operando.unwrap();
    let tipo_operando = format!("{}", operando.tipo);
    let loc = operador.localizacao;
    let operador = operador.tipo.clone();

    let par = (operador.clone(), tipo_operando.as_str());

    let compativel = match par {
        (TipoToken::Menos, "int" | "float") => true,
        (TipoToken::Not, "bool") => true,
        (TipoToken::BitNot, "int") => true,
        _ => false,
    };

    if !compativel {
        erro(
            loc,
            &format!(
                "O operador `{}` não suporta o tipo `{}`",
                operador, tipo_operando
            ),
            &[],
        );
    }
}

fn checar_calc(contexto: &mut Contexto, lhs: &Expr, op: &Token, rhs: &Expr, ast: &Ast) {
    checar_expr(contexto, lhs, ast);
    let lhs = contexto.ultimo_tipo.clone();

    checar_expr(contexto, rhs, ast);
    let rhs = contexto.ultimo_tipo.clone();

    if lhs.is_none() {
        erro(
            op.localizacao,
            "Operando à esquerda do operador não retorna um valor válido",
            &[],
        );
    }

    if rhs.is_none() {
        erro(
            op.localizacao,
            "Operando à direita do operador não retorna um valor válido",
            &[],
        );
    }

    let lhs = lhs.unwrap();
    let rhs = rhs.unwrap();

    if !lhs.tipo.lexema_igual(&rhs.tipo) {
        erro(
            op.localizacao,
            &format!(
                "Operando à esquerda do operador tem tipo `{}`, e o à direita tem tipo `{}`.",
                lhs, rhs
            ),
            &[],
        );
    }

    let tipo_str = format!("{}", lhs);
    let op_t = op.tipo.clone();

    let par = (op_t, tipo_str.as_str());

    let compativel: Option<&str> = match par {
        (TipoToken::DOr, "bool") => Some("bool"),
        (TipoToken::DAnd, "bool") => Some("bool"),

        (TipoToken::Or, "int") => Some("int"),
        (TipoToken::And, "int") => Some("int"),

        (TipoToken::Mais, "int") => Some("int"),
        (TipoToken::Menos, "int") => Some("int"),

        (TipoToken::Mais, "float") => Some("float"),
        (TipoToken::Menos, "float") => Some("float"),

        (TipoToken::MenorQ, "int" | "float") => Some("bool"),
        (TipoToken::MaiorQ, "int" | "float") => Some("bool"),
        (TipoToken::MenorIgualQ, "int" | "float") => Some("bool"),
        (TipoToken::MaiorIgualQ, "int" | "float") => Some("bool"),

        (TipoToken::Multi, "int") => Some("int"),
        (TipoToken::Div, "int") => Some("int"),
        (TipoToken::Mod, "int") => Some("int"),

        (TipoToken::Multi, "float") => Some("float"),
        (TipoToken::Div, "float") => Some("float"),
        (TipoToken::Mod, "float") => Some("float"),

        (TipoToken::Igual, "str" | "char" | "int" | "bool" | "float") => Some("bool"),
        (TipoToken::Diferente, "str" | "char" | "int" | "bool" | "float") => Some("bool"),

        _ => None,
    };

    if let Some(tipo) = compativel {
        contexto.ultimo_tipo = Some(Token {
            localizacao: op.localizacao,
            tipo: TipoToken::ID(tipo.into()),
        });
    } else {
        erro(
            op.localizacao,
            &format!("O operador `{}` não suporta o tipo `{}`", op.tipo, tipo_str),
            &[],
        );
    }
}

fn checar_expr(contexto: &mut Contexto, expr: &Expr, ast: &Ast) {
    match expr {
        Expr::Chamada {
            id,
            params,
            eh_funcao,
        } => checar_chamada(contexto, *eh_funcao, id, params, ast),
        Expr::Unaria { operador, operando } => checar_unaria(contexto, operador, operando, ast),
        Expr::Calc { lhs, op, rhs } => checar_calc(contexto, lhs, op, rhs, ast),
    }
}

fn checar_return(contexto: &mut Contexto, loc: &Localizacao, ret_expr: &Option<Expr>, ast: &Ast) {
    if let TipoToken::ID(nome_tipo) = contexto.tipo_ret.clone().tipo {
        if nome_tipo == "void" && ret_expr.is_none() {
            return;
        }

        checar_expr(contexto, ret_expr.as_ref().unwrap(), ast);
        let tipo_retornado = contexto.ultimo_tipo.as_ref();

        if let Some(t) = tipo_retornado {
            contexto.retornou = true;
            if let TipoToken::ID(nome_tipo_retornado) = t.tipo.clone() {
                if nome_tipo != nome_tipo_retornado {
                    erro(
                        *loc,
                        &format!(
                            "`{}` era esperado, mas `{}` foi retornado",
                            nome_tipo, nome_tipo_retornado
                        ),
                        &[],
                    )
                }
            } else {
                erro(
                    *loc,
                    &format!(
                        "`{}` era esperado, mas um token inválido `{}` foi retornado",
                        nome_tipo, t
                    ),
                    &[],
                )
            }
        } else {
            erro(
                *loc,
                &format!("`{}` era esperado, mas nada foi retornado", nome_tipo),
                &[],
            )
        }
    }
}

fn checar_var_decl(
    contexto: &mut Contexto,
    localizacao: &Localizacao,
    nome: &Token,
    tipo: &Token,
    valor: &Expr,
    ast: &Ast,
) {
    checar_expr(contexto, valor, ast);
    let tipo_retornado = contexto.ultimo_tipo.as_ref();

    if let Some(token_retorno) = tipo_retornado {
        if tipo.tipo.lexema_igual(&token_retorno.tipo) {
            contexto
                .simbolos_definidos
                .push(StackDefSimbolo::Simb(nome.clone(), tipo.clone()));
        } else {
            // Não são o mesmo tipo
            erro(
            *localizacao,
            &format!("A variavel `{}` esperava um valor de tipo `{}`, mas um valor de tipo `{}` foi encontrado.", nome, tipo, token_retorno),
            &[],
        );
        }
    } else {
        erro(
            *localizacao,
            "Não faz sentido atribuir uma variavel à um valor void...",
            &["var as void = funcao_que_retorna_void(); <-- isso não faz sentido"],
        );
    }
}

fn checar_var_assgn(
    contexto: &mut Contexto,
    localizacao: &Localizacao,
    nome: &Token,
    valor: &Expr,
    ast: &Ast,
) {
    checar_expr(contexto, valor, ast);
    let tipo_retornado = contexto.ultimo_tipo.as_ref();

    if let Some(token_retorno) = tipo_retornado {
        let mut tipo: Option<TipoToken> = None;

        for var in contexto.simbolos_definidos.iter().rev() {
            if let StackDefSimbolo::Simb(nome_stack, tipo_stack) = var {
                if nome_stack.tipo.lexema_igual(&nome.tipo) {
                    tipo.replace(tipo_stack.tipo.clone());
                    break;
                }
            }
        }

        if let Some(tipo) = tipo {
            if tipo.lexema_igual(&token_retorno.tipo) {
                // São do mesmo tipo
                return;
            } else {
                // Não são do mesmo tipo
                erro(
                     *localizacao,
                     &format!("A variavel `{}` esperava um valor de tipo `{}`, mas um valor de tipo `{}` foi encontrado.", nome, tipo, token_retorno),
                     &[],
               );
            }
        } else {
            erro(
                *localizacao,
                &format!(
                    "Impossivel atribuir um valor à `{}`, que ainda não foi declarada",
                    nome
                ),
                &[],
            );
        }
    } else {
        erro(
            *localizacao,
            "Não faz sentido atribuir uma variavel à um valor void...",
            &["var = funcao_que_retorna_void(); <-- isso não faz sentido"],
        );
    }
}

fn checar_printk(contexto: &mut Contexto, loc: &Localizacao, expr: &Expr, ast: &Ast) {
    checar_expr(contexto, expr, ast);
    let tipo_retornado = contexto.ultimo_tipo.as_ref();

    if let Some(token_retorno) = tipo_retornado {
        match token_retorno.tipo.clone() {
            TipoToken::ID(nome_tipo_retornado) => match nome_tipo_retornado.as_str() {
                "str" | "char" | "bool" | "int" | "float" => { /* Tipo ok */ }
                _ => erro(
                    *loc,
                    &format!("`printk` não suporta o tipo `{}`", nome_tipo_retornado),
                    &["Você pode escrever funções proprias para printar o tipo"],
                ),
            },
            _ => {
                unreachable!("Parser não conseguiu garantir que o tipo é um `id`");
            }
        }
    } else {
        erro(
            *loc,
            "`printk()` não funciona",
            &["Para printar uma nova linha basta `printk(\"\\n\")`"],
        );
    }
}

fn checar_if(
    contexto: &mut Contexto,
    localizacao: &Localizacao,
    condicao: &Expr,
    bloco_true: &Corpo,
    bloco_false: &Option<Corpo>,
    ast: &Ast,
) {
    checar_expr(contexto, condicao, ast);
    let tipo_retornado = contexto.ultimo_tipo.as_ref();

    if let Some(token_retorno) = tipo_retornado {
        match token_retorno.tipo.clone() {
            TipoToken::ID(nome_tipo_retornado) => {
                if nome_tipo_retornado != "bool" {
                    erro(token_retorno.localizacao,
                         &format!("Expressão condicional do if deveria ser booleana, mas o tipo `{}` foi encontrado no lugar", nome_tipo_retornado), &[])
                }
            }
            _ => {
                unreachable!("Parser não conseguiu garantir que o tipo é um `id`")
            }
        }

        checar_bloco(contexto, bloco_true, ast);
        if let Some(bloco_false) = bloco_false {
            checar_bloco(contexto, bloco_false, ast);
        }
    } else {
        erro(
            *localizacao,
            "Expressão condicional do if deveria ao menos retornar um valor...",
            &["`if()` não funciona..."],
        ); // a expressão deveria ao menos ter retornado algo
    }
}

fn checar_while(
    contexto: &mut Contexto,
    localizacao: &Localizacao,
    condicao: &Expr,
    bloco: &Corpo,
    ast: &Ast,
) {
    checar_expr(contexto, condicao, ast);
    let tipo_retornado = contexto.ultimo_tipo.as_ref();

    if let Some(token_retorno) = tipo_retornado {
        match token_retorno.tipo.clone() {
            TipoToken::ID(nome_tipo_retornado) => {
                if nome_tipo_retornado != "bool" {
                    erro(token_retorno.localizacao,
                         &format!("Expressão condicional do while deveria ser booleana, mas o tipo `{}` foi encontrado no lugar", nome_tipo_retornado), &[])
                }
            }
            _ => {
                unreachable!("Parser não conseguiu garantir que o tipo é um `id`")
            }
        }

        checar_bloco(contexto, bloco, ast);
    } else {
        erro(
            *localizacao,
            "Expressão condicional do while deveria ao menos retornar um valor...",
            &["`while()` não funciona..."],
        ); // a expressão deveria ao menos ter retornado algo
    }
}

fn checar_stmt(contexto: &mut Contexto, stmt: &Stmt, ast: &Ast) {
    match stmt {
        Stmt::VarDecl {
            loc,
            nome,
            tipo,
            valor,
        } => checar_var_decl(contexto, loc, nome, tipo, valor, ast),
        Stmt::VarAssgn { loc, nome, valor } => checar_var_assgn(contexto, loc, nome, valor, ast),
        Stmt::If {
            loc,
            condicao,
            bloco_true,
            bloco_false,
        } => checar_if(contexto, loc, condicao, bloco_true, bloco_false, ast),
        Stmt::While {
            loc,
            condicao,
            bloco,
        } => checar_while(contexto, loc, condicao, bloco, ast),
        Stmt::Break => {}
        Stmt::Continue => {}
        Stmt::Printk(loc, expr) => checar_printk(contexto, loc, expr, ast),
        Stmt::Return(loc, expr) => checar_return(contexto, loc, expr, ast),
        Stmt::ExprVazia(expr) => checar_expr(contexto, expr, ast),
    }
}

fn checar_bloco(contexto: &mut Contexto, bloco: &Corpo, ast: &Ast) {
    contexto.simbolos_definidos.push(StackDefSimbolo::DivFrame);
    for stmt in bloco.stmts.iter() {
        checar_stmt(contexto, stmt, ast);
    }
    desalocar_frame(contexto);
}

fn checar_func(f: &Func, ast: &Ast) {
    let mut contexto = Contexto {
        ultimo_tipo: None,
        tipo_ret: f.ret.clone(),
        simbolos_definidos: vec![StackDefSimbolo::DivFrame],
        retornou: false,
        deve_retornar: false,
    };

    if let TipoToken::ID(nome_tipo) = contexto.tipo_ret.clone().tipo {
        if nome_tipo != "void" {
            contexto.deve_retornar = true;
        }
    }

    for (nome, tipo) in f.args.iter() {
        contexto
            .simbolos_definidos
            .push(StackDefSimbolo::Simb(nome.clone(), tipo.clone()));
    }

    checar_bloco(&mut contexto, &f.corpo, ast);
    desalocar_frame(&mut contexto);

    let mut str_err: String = "".into();
    let mut return_invalido = false;

    if let TipoToken::ID(nome_funcao) = f.nome.tipo.clone() {
        if contexto.deve_retornar && !contexto.retornou {
            str_err = format!(
                "Função `{}` deveria retornar um `{:?}`, mas não retornou nada.",
                nome_funcao, f.ret
            );
            return_invalido = true;
        }

        if !contexto.deve_retornar && contexto.retornou {
            str_err = format!(
                "Função `{}` não deveria nada, mas retornou um `{:?}`.",
                nome_funcao, f.ret
            );
            return_invalido = true;
        }
    }

    if return_invalido {
        erro(f.ret.clone().localizacao, &str_err, &[]);
    }
}

pub(crate) fn checar_integridade(ast: &Ast) {
    for f in ast.funcs.iter() {
        checar_func(f, ast);
    }
}
