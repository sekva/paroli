use crate::{
    token::{Localizacao, TipoToken, Token},
    util::erro,
};

fn processar_reservada(token: &Token) -> Token {
    match token.tipo.clone() {
        TipoToken::ID(id) => {
            let localizacao = token.localizacao;
            let tipo = match id.as_str() {
                "if" => TipoToken::IF,
                "else" => TipoToken::ELSE,
                "func" => TipoToken::FUNC,
                "printk" => TipoToken::PRINTK,
                "return" => TipoToken::RETURN,
                "returns" => TipoToken::RETURNS,
                "as" => TipoToken::AS,
                "while" => TipoToken::WHILE,
                "break" => TipoToken::BREAK,
                "continue" => TipoToken::CONTINUE,
                "false" => TipoToken::BOOL(false),
                "true" => TipoToken::BOOL(true),
                _ => TipoToken::ID(id),
            };
            Token { localizacao, tipo }
        }
        _ => token.clone(),
    }
}

fn processar_reservadas(tokens: Vec<Token>) -> Vec<Token> {
    tokens
        .iter()
        .map(|token| processar_reservada(token))
        .collect()
}

pub fn lexer(entrada: &str) -> Vec<Token> {
    let mut ret = Vec::new();
    let mut entrada: Vec<char> = entrada.chars().into_iter().rev().collect();

    let mut linha = 1;
    let mut coluna = 0;

    while let Some(c) = entrada.pop() {
        coluna += 1;
        match c {
            '\n' => {
                linha += 1;
                coluna = 0;
            }

            ',' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::Virgula,
            }),

            '*' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::Multi,
            }),

            '%' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::Mod,
            }),

            '/' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::Div,
            }),

            '+' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::Mais,
            }),

            '-' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::Menos,
            }),

            ';' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::PontoVirgula,
            }),

            '(' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::ParenteseEsquerdo,
            }),

            ')' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::ParenteseDireito,
            }),

            '{' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::ChaveEsquerda,
            }),

            '}' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::ChaveDireita,
            }),

            '~' => ret.push(Token {
                localizacao: Localizacao { linha, coluna },
                tipo: TipoToken::BitNot,
            }),

            '&' => {
                let mut t = Token {
                    localizacao: Localizacao { linha, coluna },
                    tipo: TipoToken::And,
                };

                if let Some(ch) = entrada.last() {
                    if *ch == '&' {
                        entrada.pop();
                        coluna += 1;
                        t.tipo = TipoToken::DAnd;
                    }
                }

                ret.push(t)
            }

            '|' => {
                let mut t = Token {
                    localizacao: Localizacao { linha, coluna },
                    tipo: TipoToken::Or,
                };

                if let Some(ch) = entrada.last() {
                    if *ch == '|' {
                        entrada.pop();
                        coluna += 1;
                        t.tipo = TipoToken::DOr;
                    }
                }

                ret.push(t)
            }

            '=' => {
                let mut t = Token {
                    localizacao: Localizacao { linha, coluna },
                    tipo: TipoToken::Assign,
                };

                if let Some(ch) = entrada.last() {
                    if *ch == '=' {
                        entrada.pop();
                        coluna += 1;
                        t.tipo = TipoToken::Igual;
                    }
                }

                ret.push(t)
            }

            '!' => {
                let mut t = Token {
                    localizacao: Localizacao { linha, coluna },
                    tipo: TipoToken::Not,
                };

                if let Some(ch) = entrada.last() {
                    if *ch == '=' {
                        entrada.pop();
                        coluna += 1;
                        t.tipo = TipoToken::Diferente;
                    }
                }

                ret.push(t)
            }

            '<' => {
                let mut t = Token {
                    localizacao: Localizacao { linha, coluna },
                    tipo: TipoToken::MenorQ,
                };

                if let Some(ch) = entrada.last() {
                    if *ch == '=' {
                        entrada.pop();
                        coluna += 1;
                        t.tipo = TipoToken::MenorIgualQ;
                    }
                }

                ret.push(t)
            }

            '>' => {
                let mut t = Token {
                    localizacao: Localizacao { linha, coluna },
                    tipo: TipoToken::MaiorQ,
                };

                if let Some(ch) = entrada.last() {
                    if *ch == '=' {
                        entrada.pop();
                        coluna += 1;
                        t.tipo = TipoToken::MaiorIgualQ;
                    }
                }

                ret.push(t)
            }

            '"' => {
                let mut t_str: Vec<char> = Vec::new();
                let coluna_inicial = coluna + 1;
                let linha_inicial = linha;

                loop {
                    if let Some(ss) = entrada.pop() {
                        coluna += 1;
                        if ss == '"' {
                            break;
                        }

                        t_str.push(ss);
                    } else {
                        erro(
                            Localizacao {
                                linha: linha_inicial,
                                coluna: coluna_inicial,
                            },
                            "String não fechada com \"",
                            &[],
                        );
                    }
                }

                let t_str = t_str.iter().collect();
                // TODO: escape chars

                ret.push(Token {
                    localizacao: Localizacao {
                        linha: linha_inicial,
                        coluna: coluna_inicial,
                    },
                    tipo: TipoToken::STR(t_str),
                })
            }

            '\'' => {
                if let Some(ch) = entrada.pop() {
                    coluna += 1;
                    ret.push(Token {
                        localizacao: Localizacao { linha, coluna },
                        tipo: TipoToken::CHAR(ch),
                    });

                    let fecha_aspa = entrada.pop();
                    coluna += 1;

                    if fecha_aspa != Some('\'') {
                        erro(
                            Localizacao { linha, coluna },
                            "char com mais de um caracter",
                            &[],
                        );
                    }
                }
            }

            '0'..='9' => {
                let mut t_str: Vec<char> = Vec::new();
                let coluna_inicial = coluna;
                t_str.push(c);

                loop {
                    if let Some(&ch) = entrada.last() {
                        if ch == '.' || ch.is_numeric() || ch.is_ascii_hexdigit() || ch == 'x' {
                            t_str.push(ch);
                            entrada.pop();
                            coluna += 1;
                        } else {
                            break;
                        }
                    }
                }

                let t_str: String = t_str.iter().collect();

                let localizacao = Localizacao {
                    linha,
                    coluna: coluna_inicial,
                };

                if t_str.contains('.') {
                    let n = t_str.parse::<f64>();

                    if n.is_err() {
                        erro(localizacao, "Número invalido", &[]);
                    }
                    let tipo = TipoToken::FLOAT(n.unwrap());
                    ret.push(Token { localizacao, tipo })
                } else {
                    let tipo;
                    let n;

                    if t_str.contains('x') {
                        n = i64::from_str_radix(t_str.trim_start_matches("0x"), 16);
                    } else {
                        n = t_str.parse::<i64>();
                    }

                    if n.is_err() {
                        erro(localizacao, "Número invalido", &[]);
                    }
                    tipo = TipoToken::INT(n.unwrap());
                    ret.push(Token { localizacao, tipo })
                }
            }

            _ => {
                if !c.is_whitespace() && (c.is_alphabetic() || c == '_') {
                    let mut t_str: Vec<char> = Vec::new();
                    let coluna_inicial = coluna;
                    t_str.push(c);

                    while let Some(ch) = entrada.pop() {
                        coluna += 1;
                        if !(ch.is_alphanumeric() || ch == '_' || ch.is_alphabetic()) {
                            entrada.push(ch);
                            coluna -= 1;
                            break;
                        }
                        t_str.push(ch);
                    }

                    let t_str = t_str.iter().collect();
                    ret.push(Token {
                        localizacao: Localizacao {
                            linha,
                            coluna: coluna_inicial,
                        },
                        tipo: TipoToken::ID(t_str),
                    })
                }
            }
        }
    }

    ret.push(Token {
        localizacao: Localizacao { linha, coluna },
        tipo: TipoToken::EOF,
    });

    processar_reservadas(ret)
}
